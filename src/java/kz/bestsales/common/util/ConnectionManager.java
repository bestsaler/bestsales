package kz.bestsales.common.util;

import javax.faces.bean.ApplicationScoped;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by shef on 26.02.2015.
 */
@ApplicationScoped
public class ConnectionManager implements Serializable {
    private static final long serialVersionUID = 1L;
    Connection connection;

    public ConnectionManager() {
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Connection getConnection() {
        try {
            this.connection = DriverManager.getConnection("jdbc:postgresql://bestsales-remotedb.cnprkvwwwhr2.us-west-2.rds.amazonaws.com:5432/bestsales_remote", "admin_admin", "admin_admin");
        } catch (SQLException e) {
            System.err.println("Connection Failed!");
            e.printStackTrace();
        }
        if (this.connection != null) {
            System.out.println("Connection is successful!");
        } else {
            System.out.println("Failed to make connection!");
        }
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
