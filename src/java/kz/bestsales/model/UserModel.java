package kz.bestsales.model;

import kz.bestsales.common.model.AbstractModel;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by shef on 24.02.2015.
 */
@XmlRootElement
public class UserModel extends AbstractModel {
    private String user_id;
    private String user_sex;
    private String user_last_name;
    private String user_first_name;
    private String user_middle_name;
    private String user_password;
    private String user_email;
    private String user_avatar;
    private String user_mstreet_name;
    private String user_mhouse_number;
    private String user_mhouse_liter;
    private String user_astreet_name;
    private String user_bday;
    private String user_reg_date;
    private String user_is_active;

    public UserModel() {
    }

    public UserModel(String user_id, String user_sex, String user_last_name, String user_first_name,
                     String user_middle_name, String user_password, String user_email, String user_avatar,
                     String user_mstreet_name, String user_mhouse_number, String user_mhouse_liter,
                     String user_astreet_name, String user_bday, String user_reg_date, String user_is_active) {
        this.user_id = user_id;
        this.user_sex = user_sex;
        this.user_last_name = user_last_name;
        this.user_first_name = user_first_name;
        this.user_middle_name = user_middle_name;
        this.user_password = user_password;
        this.user_email = user_email;
        this.user_avatar = user_avatar;
        this.user_mstreet_name = user_mstreet_name;
        this.user_mhouse_number = user_mhouse_number;
        this.user_mhouse_liter = user_mhouse_liter;
        this.user_astreet_name = user_astreet_name;
        this.user_bday = user_bday;
        this.user_reg_date = user_reg_date;
        this.user_is_active = user_is_active;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getUser_sex() {
        return user_sex;
    }

    public void setUser_sex(String user_sex) {
        this.user_sex = user_sex;
    }

    public String getUser_last_name() {
        return user_last_name;
    }

    public void setUser_last_name(String user_last_name) {
        this.user_last_name = user_last_name;
    }

    public String getUser_first_name() {
        return user_first_name;
    }

    public void setUser_first_name(String user_first_name) {
        this.user_first_name = user_first_name;
    }

    public String getUser_middle_name() {
        return user_middle_name;
    }

    public void setUser_middle_name(String user_middle_name) {
        this.user_middle_name = user_middle_name;
    }

    public String getUser_password() {
        return user_password;
    }

    public void setUser_password(String user_password) {
        this.user_password = user_password;
    }

    public String getUser_email() {
        return user_email;
    }

    public void setUser_email(String user_email) {
        this.user_email = user_email;
    }

    public String getUser_avatar() {
        return user_avatar;
    }

    public void setUser_avatar(String user_avatar) {
        this.user_avatar = user_avatar;
    }

    public String getUser_mstreet_name() {
        return user_mstreet_name;
    }

    public void setUser_mstreet_name(String user_mstreet_name) {
        this.user_mstreet_name = user_mstreet_name;
    }

    public String getUser_mhouse_number() {
        return user_mhouse_number;
    }

    public void setUser_mhouse_number(String user_mhouse_number) {
        this.user_mhouse_number = user_mhouse_number;
    }

    public String getUser_mhouse_liter() {
        return user_mhouse_liter;
    }

    public void setUser_mhouse_liter(String user_mhouse_liter) {
        this.user_mhouse_liter = user_mhouse_liter;
    }

    public String getUser_astreet_name() {
        return user_astreet_name;
    }

    public void setUser_astreet_name(String user_astreet_name) {
        this.user_astreet_name = user_astreet_name;
    }

    public String getUser_bday() {
        return user_bday;
    }

    public void setUser_bday(String user_bday) {
        this.user_bday = user_bday;
    }

    public String getUser_reg_date() {
        return user_reg_date;
    }

    public void setUser_reg_date(String user_reg_date) {
        this.user_reg_date = user_reg_date;
    }

    public String getUser_is_active() {
        return user_is_active;
    }

    public void setUser_is_active(String user_is_active) {
        this.user_is_active = user_is_active;
    }
}
