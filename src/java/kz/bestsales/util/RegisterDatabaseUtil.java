package kz.bestsales.util;

import kz.bestsales.model.UserModel;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * Created by shef on 26.02.2015.
 */
public class RegisterDatabaseUtil {
    private Connection conn;
    private String lang;

    public RegisterDatabaseUtil(Connection conn, String lang) {
        this.conn = conn;
        this.lang = lang;
    }

    public void register(UserModel model) {
        Statement stmt = null;
        ResultSet rs = null;
        String sql = "select * from dic_phone_types";
        try {
            stmt = this.conn.createStatement();
            rs = stmt.executeQuery(sql);
            while (rs.next()) {
                System.out.println(rs.getString("PHONE_TYPE_CODE"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null)
                    rs.close();
                if (stmt != null)
                    stmt.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public void close(){
        try {
            this.conn.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
