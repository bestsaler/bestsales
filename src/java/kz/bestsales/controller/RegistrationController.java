package kz.bestsales.controller;

import kz.bestsales.common.controller.SenderMailController;
import kz.bestsales.common.util.ConnectionManager;
import kz.bestsales.model.UserModel;
import kz.bestsales.util.RegisterDatabaseUtil;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.event.AjaxBehaviorEvent;
import java.io.Serializable;

/**
 * Created by shef on 24.02.2015.
 */
@ManagedBean
@RequestScoped
public class RegistrationController implements Serializable {
    private static final long serialVersionUID = 1L;

    private ConnectionManager connectionManager;
    private SenderMailController mailer;
    UserModel model;

    public RegistrationController() {
    }

    public UserModel getModel() {
        if (this.model == null) {
            this.model = new UserModel();
            this.model.setUser_sex("1");
        }
        return model;
    }

    public void setModel(UserModel model) {
        this.model = model;
    }

    public void sendMail(AjaxBehaviorEvent event) {
        String code = "22";
        mailer = new SenderMailController();
        mailer.setUsername("kazakmolda@gmail.com");
        mailer.setPassword("ztcnmdgoogle2012");
        mailer.setTo(this.model.getUser_email());
        mailer.setSubject("Код подтверждения для регистрации на сайте <a href=\"#\">BestSales.kz</a>");
        mailer.setContent("<p><span style=\"font-size: 18pt;\">Добро пожаловать, " + this.model.getUser_first_name() + " " + this.model.getUser_middle_name() + "</span>!</p>\n" +
                "<p><span style=\"font-size: 12pt;\">Код для подтверждения</span>: <strong>" + code + "</strong></p>\n" +
                "<table style=\"height: 142px; margin-left: auto; margin-right: auto;\" border=\"1\" width=\"100%\"><caption><span style=\"font-size: 14pt;\">ВАШИ ДАННЫЕ</span></caption>\n" +
                "<tbody>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Фамилия</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>" + this.model.getUser_last_name() + "</strong></span></td>\n" +
                "<td>&nbsp;</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Имя</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>" + this.model.getUser_first_name() + "</strong></span></td>\n" +
                "<td>&nbsp;</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Отчество</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>" + this.model.getUser_middle_name() + "</strong></span></td>\n" +
                "<td>&nbsp;</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Дата рождения</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>22 июня 1993</strong></span></td>\n" +
                "<td>&nbsp;</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Пол</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>" + this.model.getUser_sex() + "</strong></span></td>\n" +
                "<td>&nbsp;</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Почтовый адрес</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>" + this.model.getUser_email() + "</strong></span></td>\n" +
                "<td>\n" +
                "<p>Почтовый адрес</p>\n" +
                "<p>используется в качестве</p>\n" +
                "<p>логина при входе</p>\n" +
                "</td>\n" +
                "</tr>\n" +
                "<tr>\n" +
                "<td><span style=\"font-size: 12pt;\">Пароль</span></td>\n" +
                "<td><span style=\"font-size: 12pt;\"><strong>" + this.model.getUser_password() + "</strong></span></td>\n" +
                "<td>\n" +
                "<p>Убедительно просим</p>\n" +
                "<p>запомнить пароль</p>\n" +
                "</td>\n" +
                "</tr>\n" +
                "</tbody>\n" +
                "</table>\n" +
                "<p><span style=\"font-size: 8pt;\"><span style=\"color: #ff0000;\">*</span> Если это письмо попало к Вам по <span style=\"color: #ff0000;\">ошибке</span>, пожалуйста, напишите нам обратное письмо с темой \"Ошибка\". После, удалите это письмо.</span></p>\n" +
                "<p>&copy;BestSales</p>");
        mailer.sendMail();
    }

    public void register() {
        this.connectionManager = new ConnectionManager();
        RegisterDatabaseUtil util = null;
        util = new RegisterDatabaseUtil(this.connectionManager.getConnection(), "RU");
        util.register(this.model);
        util.close();
        System.out.println(this.model.toXml());
    }
}
