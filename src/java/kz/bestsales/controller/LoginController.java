package kz.bestsales.controller;

import kz.bestsales.model.UserModel;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import java.io.Serializable;

@ManagedBean
@RequestScoped
public class LoginController implements Serializable {

    private static final long serialVersionUID = 1L;

    private UserModel model;

    public LoginController() {
        model = new UserModel();
    }

    public UserModel getModel() {
        return model;
    }

    public void setModel(UserModel model) {
        this.model = model;
    }

    public void login(){
        System.out.println("email: "+this.model.getUser_email());
        System.out.println("password: "+this.model.getUser_password());
    }
}